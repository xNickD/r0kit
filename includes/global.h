#ifndef __GLOBAL_H_
#define __GLOBAL_H_

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef ENABLE_DEBUGGING
#   define DEBUG(...) fprintf(stderr, __VA_ARGS__);
#else
#   define DEBUG(...)
#endif

#endif /* global.h */
