#ifndef __PROTOTYPES_H_
#define __PROTOTYPES_H_

#if defined(WIN32)

#elif defined(LINUX)

void r0kit_init(void);
void r0kit_exit(void);
void r0kit_free(void *var, int len);

#endif

#endif /* prototypes.h */
